# Prefix must be all lowercase letters, digits, and hyphens.
# Make sure it is at least 5 characters long.

prefix          = "<PREFIX>"
client_id       = "<YOUR_CLIENT_ID>"
subscription_id = "<YOUR_SUBSCRIPTION_ID>"
tenant_id       = "<YOUR_TENANT_ID>"
client_secret   = "<YOUR_CLIENT_SECRET>"
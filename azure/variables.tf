variable "location" {
  default     = "eastus"
  description = "Location where resources will be created"
}

variable "tags" {
  description = "Map of the tags to use for the resources that are deployed"
  type        = map(string)
  default = {
    environment = "codelab"
  }
}

variable "application_port" {
  description = "Port that you want to expose to the external load balancer"
  default     = 80
}

variable "admin_user" {
  description = "User name to use as the admin account on the VMs that will be part of the VM scale set"
  default     = "azureuser"
}

variable "admin_password" {
  description = "Administrator password for linux"
  default     = "Password123!"
}

variable "address_space" {
  description = "CIDR"
  default     = "10.0.0.0/16"
}

variable "client_id" {}
variable "subscription_id" {}
variable "tenant_id" {}
variable "client_secret" {}
variable "prefix" {}
# Variables for adjusting Scale Set
variable "number_of_instances" {
  type    = number
  default = 1
}

variable "instance_size" {
  type    = string
  default = "Standard_B1S"
}

# Variables for modifying APP
variable "height" {
  default     = "600"
  description = "Image height in pixels."
}
variable "width" {
  default     = "1000"
  description = "Image width in pixels."
}
variable "placeholder" {
  default     = "placedog.net"
  description = "Image-as-a-service URL. Some other fun ones to try are placekitten.com, placebeard.it, loremflickr.com, baconmockup.com, placeimg.com, placebear.com, placeskull.com"
}
variable "text" {
  default     = "Woof!"
  description = "Change the text above to something more meaningful"
}
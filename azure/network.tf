module "network" {
  source = "./modules/network/"

  # Settings vars for tags
  prefix = var.prefix
  tags   = var.tags

  # Variables for Resource Group
  address_space       = var.address_space
  location            = var.location
}
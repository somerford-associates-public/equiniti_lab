resource "azurerm_network_security_group" "vmss" {
  name                = "${var.prefix}-weballow-001"
  resource_group_name = module.network.vmss_resource_group_name
  location            = module.network.vmss_location

  security_rule {
    name                       = "WebServer"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}


# Data template Bash bootstrapping file
data "template_file" "script" {
  template = file("${path.module}/scripts/startup_script.sh")

  vars = {
    PLACEHOLDER = var.placeholder
    WIDTH       = var.width
    HEIGHT      = var.height
    PREFIX      = var.prefix
    TEXT        = var.text
  }
}


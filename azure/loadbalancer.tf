resource "azurerm_lb" "vmss" {
  name                = "${var.prefix}-vmss-lb"
  resource_group_name = module.network.vmss_resource_group_name
  location            = module.network.vmss_location

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = module.network.vmss_public_ip_id
  }

  tags = var.tags
}

resource "azurerm_lb_backend_address_pool" "vmss" {
  loadbalancer_id = azurerm_lb.vmss.id
  name            = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "vmss" {
  resource_group_name = module.network.vmss_resource_group_name
  loadbalancer_id     = azurerm_lb.vmss.id
  name                = "http-running-probe"
  port                = var.application_port
}

resource "azurerm_lb_rule" "lbnatrule" {
  resource_group_name            = module.network.vmss_resource_group_name
  loadbalancer_id                = azurerm_lb.vmss.id
  name                           = "http"
  protocol                       = "Tcp"
  frontend_port                  = var.application_port
  backend_port                   = var.application_port
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.vmss.id]
  frontend_ip_configuration_name = "PublicIPAddress"
  probe_id                       = azurerm_lb_probe.vmss.id
}
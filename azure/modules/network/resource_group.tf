resource "azurerm_resource_group" "vmss" {
  name     = "${var.prefix}-resource-group"
  location = var.location
  tags     = var.tags
}
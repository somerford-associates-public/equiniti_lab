resource "azurerm_subnet" "vmss" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.vmss.name
  virtual_network_name = azurerm_virtual_network.vmss.name
  address_prefixes     = [cidrsubnet(var.address_space, 4, 1)]
}
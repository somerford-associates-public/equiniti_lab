output "vmss_public_ip_fqdn" {
  value = azurerm_public_ip.vmss.fqdn
}

output "vmss_public_ip_address" {
  value = azurerm_public_ip.vmss.ip_address
}

output "vmss_location" {
  value = azurerm_resource_group.vmss.location
}

output "vmss_resource_group_name" {
  value = azurerm_resource_group.vmss.name
}

output "vmss_subnet_id" {
  value = azurerm_subnet.vmss.id
}

output "vmss_public_ip_id" {
  value = azurerm_public_ip.vmss.id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpc_name" {
  value = aws_vpc.vpc.tags.Name
}

output "subnet_ids" {
  value = aws_subnet.public.*.id
}

output "route_table_ids" {
  value = aws_route_table.public.*.id
}

output "vpc_cidr_block" {
  value = aws_vpc.vpc.cidr_block
}

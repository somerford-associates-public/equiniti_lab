resource "aws_route_table" "public" {
  count  = length(var.availability_zones)
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "${var.prefix}-route-table-public-${element(var.availability_zones, count.index)}"
  }
}

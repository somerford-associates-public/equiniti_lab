resource "aws_subnet" "public" {
  count             = length(var.availability_zones)
  vpc_id            = aws_vpc.vpc.id
  availability_zone = element(var.availability_zones, count.index)
  cidr_block        = cidrsubnet(var.cidr_block, 1, count.index)

  tags = {
    Name        = "${var.prefix}-public-subnet-${element(var.availability_zones, count.index)}"
  }
}

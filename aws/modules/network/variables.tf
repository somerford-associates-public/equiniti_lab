variable "cidr_block" {}

variable "aws_region" {}

variable "enabled" {
  description = "Set to false to prevent the module from creating any resources"
  default     = "true"
}

variable "availability_zones" {
  type = list(string)
}

variable "domain_name" {}

variable "domain_name_servers" {}

# Tagging variables

variable "prefix" {}


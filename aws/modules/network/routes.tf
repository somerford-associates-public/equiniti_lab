resource "aws_route" "public_internet_gateway" {
  count                  = length(var.availability_zones)
  route_table_id         = element(aws_route_table.public.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id

   depends_on = [
     aws_route_table.public,
   ]
}

resource "aws_lb" "sa_lab_lb" {
  name               = "${var.prefix}-lb"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = tolist(module.network.subnet_ids)
}

resource "aws_lb_listener" "sa_lab_lb_listener" {
  load_balancer_arn = aws_lb.sa_lab_lb.arn
  protocol          = "HTTP"
  port              = "80"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.sa_lab_lb_target_group.arn
  }
}
resource "aws_lb_target_group" "sa_lab_lb_target_group" {
  port     = "80"
  protocol = "HTTP"
  vpc_id   = module.network.vpc_id
  lifecycle {
    create_before_destroy = true
  }
}


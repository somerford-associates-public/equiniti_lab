resource "aws_security_group" "instance_sg" {
  name   = "${var.prefix}-instance-sg"
  vpc_id = module.network.vpc_id

  # Settings vars for tags
  tags = {
    Name = "${var.prefix}-instance-sg"
  }

  lifecycle {
    ignore_changes = [
      description,
    ]
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "instance_ssh_in" {
  security_group_id = aws_security_group.instance_sg.id
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["10.0.0.0/24"]
}

resource "aws_security_group_rule" "instance_http_in" {
  security_group_id = aws_security_group.instance_sg.id
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["10.0.0.0/24"]
}

resource "aws_security_group_rule" "instance_outbound" {
  security_group_id = aws_security_group.instance_sg.id
  type              = "egress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group" "alb_sg" {
  name        = "${var.prefix}-alb-sg"
  vpc_id      = module.network.vpc_id
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"

  # Settings vars for tags
  tags = {
    Name = "${var.prefix}-alb-sg"
  }

  lifecycle {
    ignore_changes = [
      description,
    ]
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "alb_http_in" {
  security_group_id = aws_security_group.alb_sg.id
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "alb_outbound" {
  security_group_id = aws_security_group.alb_sg.id
  type              = "egress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}
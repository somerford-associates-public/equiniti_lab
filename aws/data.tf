data "template_file" "user_data" {
  template = file("${path.module}/scripts/startup_script")

  vars = {
    PLACEHOLDER = var.placeholder
    WIDTH       = var.width
    HEIGHT      = var.height
    PREFIX      = var.prefix
    TEXT        = var.text
  }
}

data "aws_availability_zones" "available" {
  all_availability_zones = true
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}
module "network" {
  source = "./modules/network/"

  # Settings vars for tags
  prefix = var.prefix

  # Variables for VPC build
  cidr_block         = var.vpc_cidr
  availability_zones = [data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  aws_region         = var.region

  # DHCP Options set parameters
  domain_name         = var.dns_name
  domain_name_servers = ["AmazonProvidedDNS"]
}

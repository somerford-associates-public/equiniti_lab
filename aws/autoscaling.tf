resource "aws_autoscaling_group" "sa_lab_asg" {
  name = "${aws_launch_configuration.sa_lab_lc.name}-asg"

  min_size             = var.asg_min_size
  desired_capacity     = var.asg_desired_capacity
  max_size             = var.asg_max_size
  target_group_arns    = [aws_lb_target_group.sa_lab_lb_target_group.arn]
  launch_configuration = aws_launch_configuration.sa_lab_lc.name
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = var.metrics_granularity
  vpc_zone_identifier = tolist(module.network.subnet_ids)

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.prefix}-asg"
    propagate_at_launch = true
  }
}
resource "aws_autoscaling_policy" "sa_lab_policy_up" {
  name                   = "sa_labpolicy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.sa_lab_asg.name
}
resource "aws_autoscaling_policy" "sa_lab_policy_down" {
  name                   = "web_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.sa_lab_asg.name
}

resource "aws_autoscaling_attachment" "sa_lab_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.sa_lab_asg.name
  alb_target_group_arn   = aws_lb_target_group.sa_lab_lb_target_group.arn
}
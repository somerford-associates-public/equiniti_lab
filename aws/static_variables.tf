variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/24"
}
variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "region" {}

variable "dns_name" {
  type    = string
  default = "demo.org"
}
variable "metrics_granularity" {
  default = "1Minute"
}
variable "prefix" {}

variable "aws_access_key" {}

variable "aws_secret_key" {}